package main

import (
	"fmt"
	"log"
	"os"
	"os/user"
)

var (
	version               = "0.0.1"
	usr                   = GetUser()
	home                  = usr.HomeDir
	configuratorDirectory = home + "/.configurator/"

	configsDirectory = configuratorDirectory + "configs"
	masterFile       = configuratorDirectory + "master"
	variablesFile    = configuratorDirectory + "variables"

	filesNeeded = []string{configuratorDirectory, configsDirectory, masterFile, variablesFile}
)

// IsFirstRun() checks whether this is the user's first run of Configurator
func IsFirstRun() bool {
	for _, file := range filesNeeded {
		if _, err := os.Stat(file); os.IsNotExist(err) {
			fmt.Println(file)
			return true
		}
	}
	return false
}

// GetUser() returns the current user as the machine specifies
func GetUser() *user.User {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr
}

// SetUpConfiguratorDirectory() prepares the base directory of Configurator for use
func SetUpConfiguratorDirectory() {
	fmt.Println("Setting up Configurator directory for the first time...")
	os.Mkdir(configuratorDirectory, os.FileMode(0770))
	os.Mkdir(configsDirectory, os.FileMode(0770))
	_, err := os.Create(masterFile)
	if err != nil {
		log.Fatal(err)
	}

	_, err = os.Create(variablesFile)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Done.")
}

func main() {
	fmt.Println("Configurator Version", version)

	isFirstRun := IsFirstRun()
	if isFirstRun {
		SetUpConfiguratorDirectory()
	}
}
